<?php
/**
 * ajaxQuerys.php - Handles all ajaxQuerys
 * 
 * @author splu0008
 * 
 */
if ($_POST) {
	switch($_POST['action']) {
		case "search" :
			searchPages();
			break;
		default :
			die("Only ajax Querys, Illigal Action");
	}
}
require_once dirname ( __FILE__ ) . '/../components/db.php';
function searchPages() {
	global $mysqli;
	$string = $_POST['search'];
	$sql = "SELECT * FROM `article` WHERE `Name` LIKE '%$string%' OR `Content` LIKE '%$string%'";
	$result = $mysqli -> query($sql);
	$return = "";
	if (!$result || !$result -> num_rows > 0) {
		$return = "<p>Geen overeenkomende resultaten</p>";
	} else {
		while ($row = $result -> fetch_assoc()) {
			$return .= "<a href='index.php?action=show&page=article&id='".$row['id']."'><b>".$row['Name']."</b></a><br />";
			$return .= "<p>".substr($row['Content'], 0, 100)."</p>";
			$return .= "<br /><hr /><br />";
		}
	}
	echo $return;
}