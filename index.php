<?php 
/**
 * Index.php - main page of the application. All requests should land here, except for ajax calls
 * 
 * @author Bugslayer
 * 
 */
 //Session is reuired for authorisation
session_start(); 

//Process global query vars or set default values
$action = 'show';
if (isset($_GET['action'])) {
	$action = $_GET['action'];
}

$page = 'home';
if (isset($_GET['page'])) {
	$page = $_GET['page'];
}

// Process other actions than show
if($action!='show') {
	// Try to load a controller script
	$controller_filename = 'controllers/'.$page.'.php';
	if  (file_exists($controller_filename)) {
		include $controller_filename;
		exit(0);
	} else {
		// If no controller script exists...
		die("400 - Bad request");
	}
}
?>
<!DOCTYPE html>
<html lang="en">
	<head>
		<title>Fa Reijnders</title>

		<meta charset="utf-8">
		<meta name="description" content="Casustoets Webwereld">
		<meta name="author" content="waar0003">

	    <link rel="shortcut icon" href="favicon.ico">		

		<link rel="stylesheet" type="text/css" href="css/homestyle.css"/>
		
		<!-- javascripts laden -->
		<script src="lib/jquery-2.0.3.min.js"></script>
		<script src="js/scripts.js"></script>
	</head>

	<body>
		<header>
			<div id="account">
				<?php include('components/account.php'); ?>
			</div>
		</header>
		<nav>
			<?php include('components/menu.php'); ?>
		</nav>
		<main>
			<?php 
			$view_filename = 'views/'.$page.'.php';
			if  (file_exists($view_filename))
				include $view_filename;
			else 
				include ('404.php');
			?>
		</main>

		<footer>
			<p>
				&copy; Copyright Fa Reijnders by Bugslayer &amp; splu0008
			</p>
		</footer>
	</body>
</html>

