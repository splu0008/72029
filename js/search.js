/**
 * Search.js - display search results
 * 
 * @author splu0008
 * 
 */
$(document).ready(function() {
	$('#search_text').keyup(function() {
		if($(this).val() === "") {
			$('#search_text').parent().addClass('error');
		} else if ($(this).val().length <= 2) {
			$('#search_text').parent().addClass('error');
		} else {
			$('#search_text').parent().removeClass('error');
			var searchString = $('#search_text').val();
			// Zoekstring
			$.ajax({
				type: "POST",
				url: "ajax/ajaxQuerys.php",
				data: {
					action : "search",
					search : searchString
				},
				success: function(data) {
					document.getElementById('resultlist').innerHTML = data;
				},
				error: function(XMLHttpRequest, textStatus, errorThrown) {
					alert(errorThrown);
				}
			});
		}
	});
});
