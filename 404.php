<h1>404 – File or Directory not found</h1>
<p>De opgevraagde pagina bestaat niet. Dit kan door de volgende mogelijkheden:</p>
<ol id="leadingZero">
	<li>Je als gebruiker stiekem met de queryvars in de URL hebt geknoeid.</li>
	<li>Er in het menu verwezen wordt naar een artikel dat door de beheerder net is weggehaald.</li>
	<li>Er een programmeerfout in het menu zit.</li>
</ol>
<p>Excuses voor het ongemak</p>