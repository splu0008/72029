<?php 
/**
 * Users.php - renders, well, it should render a list of users
 * 
 * @author Bugslayer
 * 
 */
 
// Check if the request is done by an authorized user. If not, show 401.php and exit
if (!isAuthenticated()) {
	include '401.php';
	exit();
} else {
	// If authorized load controller function to display users
	$controller_filename = 'controllers/users.php';
	if (file_exists($controller_filename)) {
			include $controller_filename;
	}
}
		


?>
<h1>Gebruikers</h1>
<table class="datatable">
	<tr>
		<th>Gebruikersnaam</th>
		<th>Naam</th>
		<th>Email</th>
	</tr>
	<?php echo displayUsers(); ?>
</table>

