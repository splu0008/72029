<?php
/**
 * adresgegevens.php - renders an location page with google maps view
 * 
 * @author splu0008
 * 
 */
?>
<address>
Jesse van Splunder<br /> 
<a href="mailto:splu0008@hz.nl?subject=Contact via site">splu0008@hz.nl</a><br />
Vesting 37<br />
4388 WJ, Oost-Soubrug<br />
Nederland
</address>
<iframe id="mapsLocation" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2485.1368109704977!2d3.598379715261083!3d51.47400302096625!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x47c490b3deadc1af%3A0xf660e7a3280d2c50!2sVesting+37%2C+4388+WJ+Oost-Souburg!5e0!3m2!1snl!2snl!4v1446454789754" frameborder="0" allowfullscreen></iframe>