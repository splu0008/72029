<?php
/**
 * Login.php - Controller for the login module of the application.
 * 
 * @author Bugslayer
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/db.php';
include_once dirname ( __FILE__ ) . '/../components/datatools.php';

global $action; // set in index.php

// Determine and process the action.
switch ($action) {
	case "login" :
		if (login ()) {
			header ( 'Location: index.php' );
		} else {
			header ( 'Location: index.php?action=show&page=401' );
		}
		break;
	case "logoff" :
		logoff ();
		header ( 'Location: index.php' );
		break;
	default :
		die ( "Illegal action" );
}

/**
 * Logs in a user
 * 
 * @return boolean true if and only if the login was succesful, otherwise false is returned.
 */
function login() {
	global $mysqli;
	if (isset ( $_POST ['userid'] ) && isset ( $_POST ['password'] )) {
		$username = strip_tags ( $_POST ['userid'] );
		$password = strip_tags ( $_POST ['password'] );
		$sql = "SELECT * FROM USER WHERE Userid='" . $username . "' AND Password='" . hashPassword ( $password ) . "';";
		$result = $mysqli->query ( $sql );
		if ($row = $result->fetch_assoc ()) {
			$_SESSION ['loggedin'] = true;
			// Store userid for making orders
			$_SESSION ['Userid'] = $row ['Userid'];
			// Build and store a readble name to show in the header of the page
			$readablename = $row ['Name_first'] . " ";
			if (strlen ( $row ['Name_middle'] ) > 0)
				$readablename .= $row ['Name_middle'] . " ";
			$readablename .= $row ['Name_last'];
			$_SESSION ['readablename'] = $readablename;
			return true;
		}
		return false;
	}
	return false;
}

/**
 * Logs off a user
 */
function logoff() {
	unset ( $_SESSION ['loggedin'] );
	unset ( $_SESSION ['username'] );
	unset ( $_SESSION ['readablename'] );
}

?>