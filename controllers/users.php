<?php
/**
 * Users.php - Display users
 * 
 * @author splu0008
 * 
 */
// Include required external scripts
require_once dirname ( __FILE__ ) . '/../components/db.php';

global $action; // set in index.php
                
// Determine and process the action.
// switch ($action) {
	// case "save" :
		// register_user ();
		// header ( "Location: index.php" );
		// break;
	// default :
		// die ( "Illegal action" );
// }

/*
 * Lists all users in a table
 */
function displayUsers() {
	global $mysqli;
	$sql = "SELECT `Userid`,`Name_first`,`Name_middle`,`Name_last`,`Name_last`,`Email` FROM `user` ORDER BY `Name_last` ASC";
	$result = $mysqli->query($sql);
	$return = "";
	if (!$result) {
		die ( "Errormessage: " . $mysqli->error );
	}
	if (!$result -> num_rows > 0) {
		$return = "<tr><td colspan='3'>Geen gebruikers gevonden</td></tr>";
	}
	while ($row = $result -> fetch_assoc()) {
		$return .= "<tr><td>".$row['Userid']."</td><td>".$row['Name_first'].(($row['Name_middle'] != "")?" ".$row['Name_middle']." ":" ").$row['Name_last']."</td><td><a href='mailto:".$row['Email']."'>".$row['Email']."</td></tr>";
	}
	return $return;
}